<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class CartController extends Controller
{
    function shareData()
    {
        $categories = Category::all();
        $count_carts = Cart::where('user_id', Auth::id())->count();
        View::share(['categories' => $categories, 'count_carts' => $count_carts]);
    }

    function getCart()
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        $this->shareData();
        $carts = Cart::where('user_id', Auth::id())->get();
        return view('client.pages.cart', ['carts' => $carts]);
    }

    function postAddCart(Request $request)
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        $cart = Cart::where([['user_id', Auth::id()], ['product_id', $request->product_id]])->first();
        if ($cart) {
            $cart->quantity = $request->quantity;
            $cart->save();
        } elseif (Product::find($request->product_id)) {
            $cart = new Cart();
            $cart->user_id = Auth::id();
            $cart->product_id = $request->product_id;
            $cart->quantity = $request->quantity;
            $cart->save();
        }
        return back();
    }

    function postDeleteCart(Request $request)
    {
        if (!Auth::check()) {
            return redirect('login');
        }
        $cart = Cart::where([['user_id', Auth::id()], ['product_id', $request->product_id]])->first();
        if ($cart) {
            $cart->delete();
        }
        return back();
    }
}
