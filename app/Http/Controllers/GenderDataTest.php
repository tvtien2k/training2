<?php

namespace App\Http\Controllers;

use App\Image;
use App\Product;
use Faker\Factory;
use Illuminate\Http\Request;

class GenderDataTest extends Controller
{
    function genderProducts()
    {
        $faker = Factory::create();
        for ($i = 1; $i < 50; $i++) {
            $product = new Product();
            $product->code = strtoupper($faker->word);
            $product->name = $faker->sentence(2);
            $product->description = $faker->sentence($nbWords = 6, $variableNbWords = true);
            $product->category_id = $faker->numberBetween(1, 4);
            $product->price = $faker->numberBetween(100000, 1000000);
            $product->is_top = $faker->numberBetween(0, 1);
            $product->on_sale = $faker->numberBetween(0, 90000);
            $product->save();
        }
    }

    function genderImages()
    {
        $faker = Factory::create();
        for ($i = 1; $i < 100; $i++) {
            $image = new Image();
            $image->path = 'image/product/0bpAvMdTVynvLP.jpg';
            $image->product_id = $faker->numberBetween(1, 49);
            $image->save();
        }
    }
}
