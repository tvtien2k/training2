<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ClientController extends Controller
{
    function shareData()
    {
        $categories = Category::all();
        $count_carts = Cart::where('user_id', Auth::id())->count();
        View::share(['categories' => $categories, 'count_carts' => $count_carts]);
    }

    function getHome()
    {
        $this->shareData();
        $new_products = Product::latest()->limit(8)->get();
        $best_sell_products = Product::where('is_top', 1)->latest()->limit(8)->get();
        return view('client.pages.home', ['new_products' => $new_products, 'best_sell_products' => $best_sell_products]);
    }

    function getProductDetail(Request $request)
    {
        $this->shareData();
        $product = Product::where('code', $request->code)->first();
        if ($product) {
            $products = Product::where([['category_id', $product->category->id], ['code', '!=', $request->code]])->latest()->limit(4)->get();
            return view('client.pages.product-detail', ['product' => $product, 'products' => $products]);
        } else {
            return abort(404);
        }
    }

    function getSearch(Request $request)
    {
        $this->shareData();
        $products = Product::where('code', 'LIKE', '%' . $request->key . '%')->orWhere('name', 'LIKE', '%' . $request->key . '%')->latest()->limit(8)->paginate(12);
        return view('client.pages.search', ['products' => $products]);
    }
}
