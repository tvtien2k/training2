<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/data', 'GenderDataTest@genderImages');

Route::get('/', 'ClientController@getHome');
Route::get('/product-detail/{code}', 'ClientController@getProductDetail');
Route::get('/search', 'ClientController@getSearch');

Route::get('/cart', 'CartController@getCart');
Route::post('/add-cart', 'CartController@postAddCart');
Route::post('/delete-cart', 'CartController@postDeleteCart');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
