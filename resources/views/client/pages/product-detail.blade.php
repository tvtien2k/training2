@extends('client.index')

@section('title', 'Chi tiết sản phẩm')

@section('content')
    <div class="container mt-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$product->name}}</li>
            </ol>
        </nav>
        <section class="mb-5">
            <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @php
                                $i = 0;
                            @endphp
                            @foreach($product->images as $image)
                                @if($i==0)
                                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}"
                                        class="active"></li>
                                @else
                                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}"></li>
                                @endif
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            @if($i>0)
                                @php
                                    $i = 0;
                                @endphp
                                @foreach($product->images as $image)
                                    @if($i==0)
                                        <div class="carousel-item active">
                                            <img class="d-block img-fluid w-100" src="{{$image->path}}">
                                        </div>
                                    @else
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid w-100" src="{{$image->path}}">
                                        </div>
                                    @endif
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <img class="card-img-top" src="http://placehold.it/250x150?text=No+Image" alt="">
                            @endif
                        </div>
                        @if($i>1)
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <h5>{{$product->name}}</h5>
                    <p class="mb-2 text-muted text-uppercase small">{{$product->category->name}}</p>
                    <p>
                        <span class="mr-1">
                            <strong>
                                @if($product->on_sale != 0)
                                    <strike>{{number_format($product->price)}}</strike> {{number_format($product->on_sale)}}
                                    VNĐ
                                @else
                                    {{number_format($product->price)}}VNĐ
                                @endif
                            </strong>
                        </span>
                    </p>
                    <p class="pt-1">
                        {{$product->description}}
                    </p>
                    <hr>
                    <form action="add-cart" method="post">
                        @csrf
                        <input value="{{$product->id}}" name="product_id" hidden>
                        <div class="table-responsive mb-2">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                <tr>
                                    <td class="pl-0 pb-0 w-25">Quantity</td>
                                </tr>
                                <tr>
                                    <td class="pl-0">
                                        <div class="number-input md-number-input">
                                            <input class="quantity" min="1" name="quantity" value="1" type="number">
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <button type="button" class="btn btn-primary btn-md mr-1 mb-2">Buy now</button>
                        <button type="submit" class="btn btn-light btn-md mr-1 mb-2"><i
                                class="fas fa-shopping-cart pr-2"></i>Add to cart
                        </button>
                    </form>

                </div>
            </div>

        </section>
        <!--Section: Block Content-->
        <hr>
        <div class="mt-5">
            <div class="row">
                <h4>Các sản phẩm liên quan</h4>
            </div>
            <div class="row">
                @foreach($products as $product)
                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3 mb-4">
                        <div class="card h-100">
                            <a href="#">
                                @if(count($product->images) > 0)
                                    @foreach($product->images as $image)
                                        <img class="card-img-top" src="{{$image->path}}" alt="">
                                        @php
                                            break;
                                        @endphp
                                    @endforeach
                                @else
                                    <img class="card-img-top" src="http://placehold.it/250x150?text=No+Image" alt="">
                                @endif
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="product-detail/{{$product->code}}">{{$product->name}}</a>
                                </h4>
                                <h6>
                                    @if($product->on_sale != 0)
                                        <strike>{{number_format($product->price)}}</strike> {{number_format($product->on_sale)}}
                                        VNĐ
                                    @else
                                        {{number_format($product->price)}}VNĐ
                                    @endif
                                </h6>
                            </div>
                            <div class="card-footer">
                                <form action="add-cart" method="post">
                                    @csrf
                                    <input value="{{$product->id}}" name="product_id" hidden>
                                    <input value="1" name="quantity" hidden>
                                    <button type="submit" class="btn btn-link btn-sm">
                                        <i class="fas fa-cart-plus"></i> Thêm vào giỏ hàng
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
