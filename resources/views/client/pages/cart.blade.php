@extends('client.index')

@section('title', 'Giỏ hàng')

@section('content')
    <div class="container">
        <div class="mt-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Giỏ hàng</li>
                </ol>
            </nav>
            <div class="row">
                <!--Section: Block Content-->
                <section>

                    <!--Grid row-->
                    <div class="row">

                        <!--Grid column-->
                        <div class="col-lg-8">

                            <!-- Card -->
                            <div class="mb-3">
                                <div class="pt-4 wish-list">
                                    <h5 class="mb-4">Giỏ hàng (<span>{{$count_carts}}</span> sản phẩm)</h5>
                                    @foreach($carts as $cart)
                                        <div class="row mb-4">
                                            <div class="col-md-5 col-lg-3 col-xl-3">
                                                <div class="view zoom overlay z-depth-1 rounded mb-3 mb-md-0">
                                                    @if(count($cart->product->images) > 0)
                                                        @foreach($cart->product->images as $image)
                                                            <img class="img-fluid w-100" src="{{$image->path}}" alt="">
                                                            @php
                                                                break;
                                                            @endphp
                                                        @endforeach
                                                    @else
                                                        <img class="img-fluid w-100"
                                                             src="http://placehold.it/250x150?text=No+Image" alt="">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-lg-9 col-xl-9">
                                                <div>
                                                    <div class="d-flex justify-content-between">
                                                        <div>
                                                            <h5>{{$cart->product->name}}</h5>
                                                            <p class="mb-3 text-muted text-uppercase small">
                                                                {{$cart->product->category->name}}
                                                            </p>
                                                        </div>
                                                        <div>
                                                            <div
                                                                class="def-number-input number-input safari_only mb-0 w-100">
                                                                <form action="add-cart" method="post"
                                                                      class="form-inline">
                                                                    @csrf
                                                                    <input value="{{$cart->product->id}}"
                                                                           name="product_id"
                                                                           hidden>
                                                                    <button
                                                                        onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                                                                        class="minus decrease btn btn-outline-secondary btn-sm">
                                                                        -
                                                                    </button>
                                                                    <input class="quantity" min="1" name="quantity"
                                                                           value="{{$cart->quantity}}"
                                                                           type="number" readonly>
                                                                    <button
                                                                        onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                                                        class="plus increase btn btn-outline-secondary btn-sm">
                                                                        +
                                                                    </button>
                                                                </form>
                                                            </div>
                                                            <small id="passwordHelpBlock"
                                                                   class="form-text text-muted text-center">
                                                                (Note, 1 piece)
                                                            </small>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div>
                                                            <form action="delete-cart" method="post">
                                                                @csrf
                                                                <input value="{{$cart->product->id}}"
                                                                       name="product_id"
                                                                       hidden>
                                                                <button type="submit"
                                                                        class="btn btn-outline-danger btn-sm">
                                                                    <i class="fas fa-trash-alt mr-1"></i> Xóa
                                                                </button>
                                                            </form>
                                                        </div>
                                                        <p class="mb-0">
                                                            <span>
                                                                <strong id="summary">
                                                                    @if($cart->product->on_sale != 0)
                                                                        <strike>{{number_format($cart->product->price)}}</strike> {{number_format($cart->product->on_sale)}}
                                                                        VNĐ
                                                                    @else
                                                                        {{number_format($cart->product->price)}}VNĐ
                                                                    @endif
                                                                </strong>
                                                            </span>
                                                        <p class="mb-0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="mb-4">
                                    @endforeach
                                </div>
                            </div>
                            <!-- Card -->
                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-lg-4">
                            <!-- Card -->
                            <div class="mb-3">
                                <div class="pt-4">
                                    <h5 class="mb-3">Tổng hóa đơn</h5>
                                    <ul class="list-group list-group-flush">
                                        @php
                                            $total = 0;
                                        @endphp
                                        @foreach($carts as $cart)
                                            <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                                                {{$cart->product->name}} ({{$cart->quantity}})
                                                <span>
                                                    @if($cart->product->on_sale != 0)
                                                        {{number_format($cart->product->on_sale * $cart->quantity)}}VNĐ
                                                        @php
                                                            $total += $cart->product->on_sale * $cart->quantity;
                                                        @endphp
                                                    @else
                                                        {{number_format($cart->product->price * $cart->quantity)}}VNĐ
                                                        @php
                                                            $total += $cart->product->on_sale * $cart->quantity;
                                                        @endphp
                                                    @endif
                                                </span>
                                            </li>
                                        @endforeach
                                        <hr class="mb-4">
                                        <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                                            <div>
                                                <strong>Tổng tiền</strong>
                                            </div>
                                            <span>
                                                <strong>
                                                    {{number_format($total)}}VNĐ
                                                </strong>
                                            </span>
                                        </li>
                                    </ul>

                                    <button type="button" class="btn btn-primary btn-block">Thanh toán</button>

                                </div>
                            </div>
                            <!-- Card -->

                        </div>
                        <!--Grid column-->

                    </div>
                    <!-- Grid row -->

                </section>
                <!--Section: Block Content-->
            </div>
        </div>
    </div>
@endsection
