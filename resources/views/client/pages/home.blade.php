@extends('client.index')

@section('title', 'Trang chủ')

@section('content')
    @include('client.layouts.slide')
    <div class="container">
        <div class="mt-5">
            <div class="row">
                <h4>Mới nhất</h4>
            </div>
            <div class="row">
                @foreach($new_products as $product)
                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3 mb-4">
                        <div class="card h-100">
                            <a href="#">
                                @if(count($product->images) > 0)
                                    @foreach($product->images as $image)
                                        <img class="card-img-top" src="{{$image->path}}" alt="">
                                        @php
                                            break;
                                        @endphp
                                    @endforeach
                                @else
                                    <img class="card-img-top" src="http://placehold.it/250x150?text=No+Image" alt="">
                                @endif
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="product-detail/{{$product->code}}">{{$product->name}}</a>
                                </h4>
                                <h6>
                                    @if($product->on_sale != 0)
                                        <strike>{{number_format($product->price)}}</strike> {{number_format($product->on_sale)}}
                                        VNĐ
                                    @else
                                        {{number_format($product->price)}}VNĐ
                                    @endif
                                </h6>
                            </div>
                            <div class="card-footer">
                                <form action="add-cart" method="post">
                                    @csrf
                                    <input value="{{$product->id}}" name="product_id" hidden>
                                    <input value="1" name="quantity" hidden>
                                    <button type="submit" class="btn btn-link btn-sm">
                                        <i class="fas fa-cart-plus"></i> Thêm vào giỏ hàng
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <hr>
        <div class="mt-5">
            <div class="row">
                <h4>Bán chạy nhất</h4>
            </div>
            <div class="row">
                @foreach($best_sell_products as $product)
                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3 mb-4">
                        <div class="card h-100">
                            <a href="#">
                                @if(count($product->images) > 0)
                                    @foreach($product->images as $image)
                                        <img class="card-img-top" src="{{$image->path}}" alt="">
                                        @php
                                            break;
                                        @endphp
                                    @endforeach
                                @else
                                    <img class="card-img-top" src="http://placehold.it/250x150?text=No+Image" alt="">
                                @endif
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="product-detail/{{$product->code}}">{{$product->name}}</a>
                                </h4>
                                <h6>
                                    @if($product->on_sale != 0)
                                        <strike>{{number_format($product->price)}}</strike> {{number_format($product->on_sale)}}
                                        VNĐ
                                    @else
                                        {{number_format($product->price)}}VNĐ
                                    @endif
                                </h6>
                            </div>
                            <div class="card-footer">
                                <form action="add-cart" method="post">
                                    @csrf
                                    <input value="{{$product->id}}" name="product_id" hidden>
                                    <input value="1" name="quantity" hidden>
                                    <button type="submit" class="btn btn-link btn-sm">
                                        <i class="fas fa-cart-plus"></i> Thêm vào giỏ hàng
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
