<footer class="bg-light text-center text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row">
            <!--Grid column-->
            <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                <h5 class="text-uppercase">ABOUT</h5>
                <p>
                    Online & physical bead shop with the best beads and beading supplies in Zimbabwe ✓ Over 9000 beads
                    for jewelry making ✓ Glass beads ✓ Beading supplies and much more!
                </p>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase">Links</h5>

                <ul class="list-unstyled mb-0">
                    <li>
                        <a href="#!" class="text-dark">Link 1</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">Link 2</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">Link 3</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">Link 4</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-0">CONTACT</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="#!" class="text-dark">C. RVM SeaMaf</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">B. 19 Caithness Road Eastlea, Harare</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">T. +263782149840</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">E. rvmseamaf@gmail.com</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        Copyright ©2021 All rights reserved | Developed By Eloquent Geeks
    </div>
    <!-- Copyright -->
</footer>
