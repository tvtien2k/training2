<ul class="nav justify-content-end">
    @auth
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
               aria-expanded="false">{{Auth::user()->name}}</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Thông tin</a>
                <a class="dropdown-item" href="#">Cài đặt</a>
                <div class="dropdown-divider"></div>
                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button class="dropdown-item" type="submit">Đăng xuất</button>
                </form>
            </div>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="cart">
                <i class="fas fa-shopping-cart"></i> Giỏ hàng ({{$count_carts}})
            </a>
        </li>
    @endauth
    @guest
        <li class="nav-item">
            <a class="nav-link" href="login">Đăng nhập</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="register">Đăng ký</a>
        </li>
    @endguest
</ul>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">TRANG CHỦ</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Danh mục
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach($categories as $category)
                        <a class="dropdown-item" href="#">{{$category->name}}</a>
                    @endforeach
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Mới nhất</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Bán chạy nhất</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" method="get" action="search">
            <input class="form-control mr-sm-2" type="search" aria-label="Search" name="key">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Tìm kiếm</button>
        </form>
    </div>
</nav>
